/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/03 00:59:27 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/10 02:03:34 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief 	Swaps two strings.
 * @cite	Exercise C 01 - 03 (modified).
 * 
 * @param a 	Fist string.
 * @param b 	Second string.
 */
void	ft_swap(char **a, char **b)
{
	char	*aux;

	aux = *a;
	*a = *b;
	*b = aux;
}

/**
 * @brief 	Compares two strings.
 * @cite	Exercise C 03 - 00.
 * 
 * @param s1 	Fist string.
 * @param s2 	Second string.
 * 
 * @return	'1' if s1 is greater than s2; '0' otherwise. 
 */
int	ft_strcmp(char *s1, char *s2)
{
	while (*s1 && *s2 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

/**
 * @brief 	Prints the program's arguments.
 * @cite	Exercise C 06 - 01.
 *
 * @param args		Arguments.
 * @param count 	Number of arguments.
 */
void	ft_print_params(char **args, int count)
{
	int	i;
	int	j;

	i = 1;
	while (i < count)
	{
		j = 0;
		while (args[i][j])
		{
			write(1, &args[i][j], 1);
			j++;
		}
		write(1, "\n", 1);
		i++;
	}
}

/**
 * @brief 	Sorts the elements of an array
 * 			according to their ASCII value.
 * @cite	Exercise C 01 - 08 (modified).
 *
 * @param args		Arguments.
 * @param count 	Number of arguments.
 */
void	ft_sort_tab(char **tab, int size)
{
	int	i;
	int	j;

	i = 0;
	while (i < size - 1)
	{
		j = i + 1;
		while (j < size)
		{
			if (0 < ft_strcmp(tab[i], tab[j]))
				ft_swap(&tab[i], &tab[j]);
			j++;
		}
		i++;
	}
}

int	main(int argc, char **argv)
{
	ft_sort_tab(argv, argc);
	ft_print_params(argv, argc);
	return (0);
}
